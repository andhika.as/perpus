from django.shortcuts import render, redirect
from .models import Perpus
#from datetime import datetime
import pytz
import json
        # Create your views here.
perpus_dict = {}
    
def index(request):
    perpus_dict = Perpus.objects.all().values()
    return render(request, 'perpus.html', {'perpus_dict' : convert_queryset_into_json(perpus_dict)})

def add_perpus(request):
    if request.method == 'POST':
        nomorin = request.POST['nomor']
        Perpus.objects.create( \
            nomor=nomorin, nama=request.POST['nama'], alamat=request.POST['alamat'], jumlahbuku=request.POST['jumlahbuku'] )
        return redirect('/cabang/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val