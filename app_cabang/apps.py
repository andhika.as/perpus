from django.apps import AppConfig


class AppCabangConfig(AppConfig):
    name = 'app_cabang'
