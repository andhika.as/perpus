from django.db import models

class Perpus(models.Model):
    nomor = models.TextField(max_length=20, primary_key=True)
    nama = models.TextField(max_length=60)
    alamat = models.TextField(max_length=100)
    jumlahbuku = models.IntegerField(default=0, null=False)

    def __str__(self):
        return self.nama
