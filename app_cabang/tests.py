from django.test import TestCase, Client
from django.urls import resolve
from .models import Perpus
from django.utils import timezone

from .views import index

class CabangPageTest(TestCase):
    def test_cabang_url_is_exist(self):
        response = Client().get('/cabang/')
        self.assertEqual(response.status_code,200)

    def test_cabang_using_cabanghtml(self):
        response = Client().get('/cabang/')
        self.assertTemplateUsed(response, 'perpus.html')

    def test_cabang_using_index_func(self):
        found = resolve('/cabang/')
        self.assertEqual(found.func, index)
    
    # def test_model_can_create_new_activity(self):
    #     #Creating a new activity
    #     new_activity = cabang.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

    #     #Retrieving all available activity
    #     counting_all_available_activity = cabang.objects.all().count()
    #     self.assertEqual(counting_all_available_activity,1)
    
    def test_can_save_a_POST_request(self):
        response = self.client.post('/cabang/add_perpus/', \
            data={'nomor': '12345', 'nama': 'Perpus Fasilkom', 'alamat' : 'Jalan Riau no.4', 'jumlahbuku': '100' })
        counting_all_available_activity = Perpus.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/cabang/')

        new_response = self.client.get('/cabang/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Perpus Fasilkom', html_response)