from django.test import TestCase, Client
from django.urls import resolve
from .models import Peminjaman
from django.utils import timezone
import datetime

from app_buku.models import Buku
from app_user.models import User


from .views import index

NOW = datetime.datetime.now()

class PeminjamanPageTest(TestCase):
    def test_peminjaman_url_is_exist(self):
        response = Client().get('/peminjaman/')
        self.assertEqual(response.status_code,200)

    def test_peminjaman_using_peminjamanhtml(self):
        response = Client().get('/peminjaman/')
        self.assertTemplateUsed(response, 'peminjaman.html')

    def test_peminjaman_using_index_func(self):
        found = resolve('/peminjaman/')
        self.assertEqual(found.func, index)
    
    # def test_model_can_create_new_activity(self):
    #     #Creating a new activity
    #     new_activity = peminjaman.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

    #     #Retrieving all available activity
    #     counting_all_available_activity = peminjaman.objects.all().count()
    #     self.assertEqual(counting_all_available_activity,1)
    
    def test_can_save_a_POST_request(self):
        
        objbuku = Buku()
        objbuku.nomorbuku = '123'
        objbuku.judul = 'tes'
        objbuku.penulis = 'aku'
        objbuku.save()

        objuser = User()
        objuser.npm = '1234567890'
        objuser.nama = 'dika'
        objuser.jurusan = 'ilkom'
        objuser.fakultas = 'fasilkom'
        objuser.save()

        tglmulai = NOW
        tglkembali = tglmulai + datetime.timedelta(days=7)
        response = self.client.post('/peminjaman/add_peminjaman/', \
            data={'nobuku': '123', 'npmuser': '1234567890', 'tglmulai' : tglmulai, 'tglkembali' : tglkembali, 'kembali' : 'False'})
        counting_all_available_activity = Peminjaman.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/peminjaman/')

        new_response = self.client.get('/peminjaman/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('123', html_response)