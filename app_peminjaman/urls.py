from django.conf.urls import url
from .views import index, add_peminjaman
#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'), 
    url(r'add_peminjaman/$', add_peminjaman, name='add_peminjaman'),
]