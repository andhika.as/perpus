from django.db import models
from app_user.models import User
from app_buku.models import Buku

    # Create your models here.
class Peminjaman(models.Model):
    nobuku = models.ForeignKey(Buku)
    npmuser = models.ForeignKey(User)
    nomorpinjam = models.AutoField(primary_key=True)
    tglmulai = models.DateTimeField()
    tglkembali = models.DateTimeField()
    #sudah kembali
    #kembali = models.BooleanField()



