from django.shortcuts import render, redirect
from .models import Peminjaman
from datetime import datetime
import pytz
import json
from django.utils import timezone
from app_buku.models import Buku
from app_user.models import User
# import datetime

        # Create your views here.
peminjaman_dict = {}
    
def index(request):
    semuabuku = Buku.objects.all()
    semuauser = User.objects.all()
    peminjaman_dict = Peminjaman.objects.all()
    return render(request, 'peminjaman.html', 
        {'peminjaman_dict' : peminjaman_dict, 
        'semuabuku':semuabuku, 
        'semuauser':semuauser
        } )

def add_peminjaman(request):
    if request.method == 'POST':
        tglmulai = request.POST.get('tglmulai')
        tglkembali = request.POST.get('tglkembali')
        nobuku = request.POST.get('nobuku')
        npm = request.POST.get('npmuser')

        objbuku = Buku.objects.get(nomorbuku=nobuku)
        objuser = User.objects.get(npm=npm)

        Peminjaman.objects.create( \
            nobuku=objbuku, npmuser=objuser, \
            tglmulai=tglmulai, tglkembali=tglkembali )

        return redirect('/peminjaman/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val