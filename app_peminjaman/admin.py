from django.contrib import admin
from app_peminjaman.models import Peminjaman

class PeminjamanAdmin(admin.ModelAdmin):
    list_display = ('nobuku','npmuser','tglmulai', 'tglkembali')

admin.site.register(Peminjaman, PeminjamanAdmin)

