from django.conf.urls import url
from .views import index, add_user
#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'), 
    url(r'add_user/$', add_user, name='add_user'),
]