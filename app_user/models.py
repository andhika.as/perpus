from django.db import models

    # Create your models here.
class User(models.Model):
    npm = models.TextField(max_length=10, primary_key=True)
    nama = models.TextField(max_length=60)
    jurusan = models.TextField(max_length=50)
    fakultas = models.TextField(max_length=50)

    def __str__(self):
        return self.nama