from django.shortcuts import render, redirect
from .models import User
#from datetime import datetime
import pytz
import json
        # Create your views here.
user_dict = {}
    
def index(request):
    user_dict = User.objects.all().values()
    return render(request, 'user.html', {'user_dict' : convert_queryset_into_json(user_dict)})

def add_user(request):
    if request.method == 'POST':
        npm_in = request.POST['npm']
        User.objects.create( \
            npm=npm_in, nama=request.POST['nama'], jurusan=request.POST['jurusan'], fakultas=request.POST['fakultas'] )
        return redirect('/user/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val