from django.test import TestCase, Client
from django.urls import resolve
from .models import User
from django.utils import timezone

from .views import index

class UserPageTest(TestCase):
    def test_user_url_is_exist(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code,200)

    def test_user_using_userhtml(self):
        response = Client().get('/user/')
        self.assertTemplateUsed(response, 'user.html')

    def test_user_using_index_func(self):
        found = resolve('/user/')
        self.assertEqual(found.func, index)
    
    # def test_model_can_create_new_activity(self):
    #     #Creating a new activity
    #     new_activity = User.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

    #     #Retrieving all available activity
    #     counting_all_available_activity = User.objects.all().count()
    #     self.assertEqual(counting_all_available_activity,1)
    
    def test_can_save_a_POST_request(self):
        response = self.client.post('/user/add_user/', \
            data={'nama': 'Si Coba Nama', 'npm': '1234567890', 'jurusan': 'Ilmu Coba Coba', 'fakultas' : 'Fakultas Rekayasa' })
        counting_all_available_activity = User.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/user/')

        new_response = self.client.get('/user/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('1234567890', html_response)