from django.conf.urls import url
from .views import index, about
#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^about/$', about, name='about'),
]