from django.shortcuts import render

home_content = 'Selamat datang di Sistem Perpustakaan by Andhika'

def index(request):
    response = { 'content': home_content}
    return render(request, 'home.html', response)

def about(request):
    return render(request,'about.html')