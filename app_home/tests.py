from django.test import TestCase, Client
from django.urls import resolve
from .views import index, home_content, about
from django.http import HttpRequest
from unittest import skip

# Create your tests here.

class HomeUnitTest(TestCase):

    def home_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code,200)

    def test_home_using_index_func(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)

    def test_home_content_is_written(self):
        #Content cannot be null
        self.assertIsNotNone(home_content)

    def test_home_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn(home_content, html_response)
        # self.assertIn(landing_page_content, html_response)

class AboutUnitTest(TestCase):
    def about_url_is_exist(self):
        response = Client().get('/home/about/')
        self.assertEqual(response.status_code,200)

    def test_about_using_index_func(self):
        found = resolve('/home/about/')
        self.assertEqual(found.func, about)