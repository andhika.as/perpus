from django.db import models
from app_cabang.models import Perpus

    # Create your models here.
class Buku(models.Model):
    nomorbuku = models.TextField(max_length=20, primary_key=True)
    judul = models.TextField(max_length=60)
    penulis = models.TextField(max_length=60)
    jumlah = models.IntegerField(default=0, null=False)
    noperpus = models.ForeignKey(Perpus, null=True)

    def __str__(self):
        return self.judul