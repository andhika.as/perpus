from django.test import TestCase, Client
from django.urls import resolve
from .models import Buku
from django.utils import timezone
from app_cabang.models import Perpus

from .views import index

class BukuPageTest(TestCase):
    def test_buku_url_is_exist(self):
        response = Client().get('/buku/')
        self.assertEqual(response.status_code,200)

    def test_buku_using_bukuhtml(self):
        response = Client().get('/buku/')
        self.assertTemplateUsed(response, 'buku.html')

    def test_buku_using_index_func(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, index)
    
    # def test_model_can_create_new_activity(self):
    #     #Creating a new activity
    #     new_activity = buku.objects.create(date=timezone.now(),activity='Aku mau latihan ngoding deh')

    #     #Retrieving all available activity
    #     counting_all_available_activity = buku.objects.all().count()
    #     self.assertEqual(counting_all_available_activity,1)
    
    def test_can_save_a_POST_request(self):
        objperpus = Perpus()
        objperpus.nomor = '123'
        objperpus.nama = 'Test nama perpus'
        objperpus.alamat = 'Jl. perpus'
        objperpus.jumlahbuku = '100'
        objperpus.save()
        
        response = self.client.post('/buku/add_buku/', \
            data={'nomorbuku': '12345', 'judul': 'Judul Buku', 'penulis' : 'Mr. Penulis', 'jumlah' : '1', 'noperpus': '123' })
        counting_all_available_activity = Buku.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/buku/')

        new_response = self.client.get('/buku/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('12345', html_response)