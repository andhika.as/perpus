from django.conf.urls import url
from .views import index, add_buku
#url for app, add your URL Configuration

urlpatterns = [
    url(r'^$', index, name='index'), 
    url(r'add_buku/$', add_buku, name='add_buku'),
]