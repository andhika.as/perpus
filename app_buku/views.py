from django.shortcuts import render, redirect
from .models import Buku
from app_cabang.models import Perpus
#from datetime import datetime
import pytz
import json
        # Create your views here.
buku_dict = {}
    
def index(request):
    buku_dict = Buku.objects.all().values()
    semuaperpus = Perpus.objects.all()
    return render(request, 'buku.html', 
        {'buku_dict' : convert_queryset_into_json(buku_dict),
        'semuaperpus' : semuaperpus
        })

def add_buku(request):
    if request.method == 'POST':
        nobuku = request.POST.get('nomorbuku')
        noperpus = request.POST['noperpus']

        objperpus = Perpus.objects.get(nomor=noperpus)

        Buku.objects.create( \
            nomorbuku=nobuku, judul=request.POST['judul'],\
            penulis=request.POST['penulis'], jumlah=request.POST['jumlah'], noperpus=objperpus )
        return redirect('/buku/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val